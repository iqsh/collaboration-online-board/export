Übersetzung der „Developer Certificate of Origin, Version 1.1“ aus dem Englischsprachigen. Der Originaltext kann unter dem nachstehenden Link eingesehen werden:  
(https://developercertificate.org [extern])[https://developercertificate.org]  
Copyright (C) 2004, 2006 The Linux Foundation and its contributors.  
  
Developer's Certificate of Origin 1.1  
  
Indem ich einen Beitrag zu diesem Projekt leiste, bestätige ich, dass:  
  
1. Der Beitrag ganz oder teilweise von mir erstellt wurde und ich das Recht habe, ihn unter der in der Datei angegebenen Open Source-Lizenz einzureichen; oder  
2. Der Beitrag auf einer früheren Arbeit basiert, die nach meinem besten Wissen unter einer geeigneten Open Source-Lizenz steht und ich das Recht habe, diese Arbeit mit Modifikationen, ob ganz oder teilweise von mir erstellt, unter der gleichen Open Source-Lizenz einzureichen (es sei denn, ich bin berechtigt, diese unter einer anderen Lizenz einreichen), wie in der Datei angegeben; oder  
3. Der Beitrag mir direkt von einer anderen Person, die (1), (2) oder (3) bestätigt hat, zur Verfügung gestellt wurde, und ich diesen nicht modifiziert habe.  
4. Ich verstehe und erkläre mich damit einverstanden, dass dieses Projekt und der Beitrag öffentlich sind und dass eine Aufzeichnung des Beitrags (einschließlich aller persönlichen Informationen, die ich zusammen mit dem Beitrag einreiche, einschließlich meiner Unterschrift) auf unbestimmte Zeit aufbewahrt wird und in Übereinstimmung mit diesem Projekt oder der/den beteiligten Open Source-Lizenz(en) weiterverteilt werden kann.