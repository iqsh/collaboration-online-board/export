### Deutsch
## Collaborative Online Board - Export
Ein Headless-Browser, der Screenshots und PDFs einer Oberfläche erstellt. Die Software benutzt *fastify* als Framework, *Objection.js / Knex.js* als Objektrelationale Abbildung (ORM) und Puppeteer als *Headless-Browser*.

### System Anforderungen:
* NodeJS 16+ 
* NPM
* Libc6 (or libc6-compat)
* Chromium
* NSS
* Freetype
* Harfbuzz
* ca-certificates
* ttf-freefont
* Getestet mit *Ubuntu 20.04 LTS* und *Alpine Linux*

### Installation von den Software-Abhängigkeiten:
Gehen Sie in den *export*-Ordner und führen Sie `npm install` aus.

### Ausführen der Anwendung:
Kopieren und benennen Sie die .env.example in .env um und passen Sie die Werte an.  
Zum Ausführen des Servers: `npm run start`

***
### English
## Collaborative Online Board - Export
Headless-Browser, which creates screenshots and PDFs from the *pane*. The software uses *fastify* as framework, *Objection.js / Knex.js* as object-relational-mapper (ORM) and *Puppeteer* as *Headless Browser*.

### System Requirements:
* NodeJS 16+ 
* NPM
* Libc6 (or libc6-compat)
* Chromium
* NSS
* Freetype
* Harfbuzz
* ca-certificates
* ttf-freefont
* Tested with *Ubuntu 20.04 LTS* and *Alpine Linux*

### Installation of the software dependencies:
Go into the *export* folder and run `npm install`. 

### Run application:
Copy and rename .env.example to .env and adapt the values.  
Run Server: `npn run start`