// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import exportRoute from './v1/export.mjs';

export default function(fastify, options, done){
    fastify.register(exportRoute,{ prefix: 'export' });
    done()
}