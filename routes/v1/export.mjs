// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import {setTimeout} from "node:timers/promises";
import puppeteer from 'puppeteer';
import * as jose from 'jose';
import Globals from "../../lib/globalvars.mjs";
import freeformSize from "../../database/models/freeformSize.mjs";
import sessionStore from "../../database/models/sessionStore.mjs";
import adminPanel from "../../database/models/adminPanel.mjs";
import aiChat from "../../database/models/aiChat.mjs";


async function autoScroll(page) {
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            let totalHeight = 0;
            let distance = 100;
            let timer = setInterval(() => {
                let scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if (totalHeight >= scrollHeight) {
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}

/**
 * Creates the token for the cookie of the user
 * @param {string} mail Mail address of user
 * @param {string} accesscode Accesscode from instance
 * @param {boolean} canUseAI If the user can use the AI
 * @returns the token
 */
export async function createToken(mail, accesscode, canUseAI) {
    let dateNight = new Date();
    if (dateNight.getUTCHours() > 2)
        dateNight = new Date(new Date(dateNight.setDate(dateNight.getDate() + 1)).setUTCHours(2)).setUTCMinutes(0);
    try {
        const jwt = await new jose.UnsecuredJWT({ 'mail': mail, 'accesscode': accesscode, 'canUseAI': canUseAI })
            .setIssuedAt()
            .setExpirationTime(dateNight)
            .encode();
        await sessionStore.query().insert({ mail: mail, expire: dateNight.toString(), session_id: jwt });
        return { jwt, date: dateNight };
    } catch (error) {
        console.error(error);
        return null;
    }
}

class InflightRequests {
    constructor(page) {
        this._page = page;
        this._requests = new Set();
        this._onStarted = this._onStarted.bind(this);
        this._onFinished = this._onFinished.bind(this);
        this._page.on('request', this._onStarted);
        this._page.on('requestfinished', this._onFinished);
        this._page.on('requestfailed', this._onFinished);

    }

    _onStarted(request) { if (this.unvalid(request.url()) === true) { this._requests.add(request) } }
    _onFinished(request) { if (this.unvalid(request.url()) === true) { this._requests.delete(request) } }

    unvalid(src) {
        const unv = ["data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAJwCAYAAABI9AvEAAACkklEQVRIS1XHdxQIZBiFcTtCkmRHqSh7U1mpJMlK9t7Jpm2WUFnZK7PsbGWWUfYohOy9NZFVznNP5/vOff/4vfdJkOD/S8iPJCJFYicJKZIGkrEi95AiuZOCFPc6KUmRyklNivsCaViR+0mR1nmAFOmcB0mR3nmIFBmcjKTIFMjMimQhRVYnGykedrKTIofzCCkeDeRkRR4jxePOE6TI5eQmxZPOU6TIE8jLiuQjRX6nACkKOoVIUdgpQoqigWKsSHFSlHBKkqKU8zQpnnGeJUXpQBlWpCwpyjnlSfGcU4EUzzsvkOJFpyIpXgpUYkVeJkVl5xVSVHFeJUVVpxopqgdqsCI1SfGaU4sUrzu1SVHHqUuKeoH6rEgDUjR0GpGisdOEFE2dZqRoHmjBirQkRSunNSnaOG1J8YbTjhRvOu1J0SHQkRXpRIrOThdSdHW6kaK78xYp3g68w4q8S4r3nPdJ8YHTgxQ9nV6k6B3ow4r0JcWHzkek6Od8TIr+zgBSDAx8wop8SorPnEGkGOwMIcVQZxgpPg8MZ0VGkGKkM4oUo50xpBjrjCPFeGcCKSYGvmBFJpFisjOFFFOdaaSY7nxJiq8CM1iRmaSY5cwmxRxnLinmOV+TYn5gASuykBSLnMWkWOIsJcUy5xtSfBtYzoqsIMVKZxUpVjtrSPGd8z0p1jrrSLE+sIEV+YEUPzobSbHJ2UyKLc5WUmwLbGdFdpBip7OLFD85P5Nit7OHFHsDv7Ai+0ix3zlAil+dg6Q45BwmxZHAUVbkGCmOOydIcdI5RYrTzhlSnA2cY0XOk+KCc5EUl5zLpLji/EaK350/SPFn4C9W5G9SXHWukeK68w8pbjg3SXErcJsVuUOKf53/7gIPXjbYDO78kgAAAABJRU5ErkJggg==",
            "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIKCSB2aWV3Qm94PSIwIDAgMjQgMjQiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDI1NSAyNTU7IiB4bWw6c3BhY2U9InByZXNlcnZlIiBmaWxsPSJXaW5kb3ciPgo8Zz4KCTxjaXJjbGUgY3g9IjEyIiBjeT0iNiIgcj0iMiIvPgoJPGNpcmNsZSBjeD0iMTIiIGN5PSIxMiIgcj0iMiIvPgoJPGNpcmNsZSBjeD0iMTIiIGN5PSIxOCIgcj0iMiIvPgo8L2c+Cjwvc3ZnPgo=", "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMSwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAxOTYgMTk2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxOTYgMTk2OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik05OCw0OXY0Yy0yNC45LDAtNDUsMjAuMS00NSw0NQoJYzAsMTgsMTAuNiwzMy42LDI1LjksNDAuOGwtMS43LDMuNmMwLjEsMCwwLjIsMC4xLDAuMywwLjFjLTAuMSwwLTAuMi0wLjEtMC4zLTAuMWwwLDBDNjAuNSwxMzQuNSw0OSwxMTcuNiw0OSw5OAoJQzQ5LDcwLjksNzAuOSw0OSw5OCw0OXoiLz4KPC9zdmc+Cg==",
            "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjIuMSwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCAxOTYgMTk2IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAxOTYgMTk2OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0xNDcsOThjMCwxOS42LTExLjUsMzYuNS0yOC4yLDQ0LjRsMCwwYy0wLjEsMC0wLjIsMC4xLTAuMywwLjEKCWMwLjEsMCwwLjItMC4xLDAuMy0wLjFsLTEuNy0zLjZDMTMyLjQsMTMxLjYsMTQzLDExNiwxNDMsOThjMC0yNC45LTIwLjEtNDUtNDUtNDV2LTRDMTI1LjEsNDksMTQ3LDcwLjksMTQ3LDk4eiIvPgo8L3N2Zz4K",
            "data:image/svg+xml;base64,PHN2ZyBmaWxsPSJXaW5kb3ciIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgd2lkdGg9IjI0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogICAgPHBhdGggZD0iTTMgOXY2aDRsNSA1VjRMNyA5SDN6bTEzLjUgM2MwLTEuNzctMS4wMi0zLjI5LTIuNS00LjAzdjguMDVjMS40OC0uNzMgMi41LTIuMjUgMi41LTQuMDJ6TTE0IDMuMjN2Mi4wNmMyLjg5Ljg2IDUgMy41NCA1IDYuNzFzLTIuMTEgNS44NS01IDYuNzF2Mi4wNmM0LjAxLS45MSA3LTQuNDkgNy04Ljc3cy0yLjk5LTcuODYtNy04Ljc3eiIvPgogICAgPHBhdGggZD0iTTAgMGgyNHYyNEgweiIgZmlsbD0ibm9uZSIvPgo8L3N2Zz4K",
            "data:image/svg+xml;base64,PHN2ZyBmaWxsPSJXaW5kb3ciIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgd2lkdGg9IjI0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogICAgPHBhdGggZD0iTTAgMGgyNHYyNEgweiIgZmlsbD0ibm9uZSIvPgogICAgPHBhdGggZD0iTTcgMTRINXY1aDV2LTJIN3YtM3ptLTItNGgyVjdoM1Y1SDV2NXptMTIgN2gtM3YyaDV2LTVoLTJ2M3pNMTQgNXYyaDN2M2gyVjVoLTV6Ii8+Cjwvc3ZnPgo=",
            "https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2",
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAADGCAYAAAAT+OqFAAAAdklEQVQoz42QQQ7AIAgEF/T/D+kbq/RWAlnQyyazA4aoAB4FsBSA/bFjuF1EOL7VbrIrBuusmrt4ZZORfb6ehbWdnRHEIiITaEUKa5EJqUakRSaEYBJSCY2dEstQY7AuxahwXFrvZmWl2rh4JZ07z9dLtesfNj5q0FU3A5ObbwAAAABJRU5ErkJggg==",
            "data:image/svg+xml;base64,PHN2ZyBmaWxsPSJXaW5kb3ciIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgd2lkdGg9IjI0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogICAgPHBhdGggZD0iTTggNXYxNGwxMS03eiIvPgogICAgPHBhdGggZD0iTTAgMGgyNHYyNEgweiIgZmlsbD0ibm9uZSIvPgo8L3N2Zz4K"]
        return !unv.includes(src);
    }

    inflightRequests() { return Array.from(this._requests); }


    dispose() {
        this._page.off('request', this._onStarted);
        this._page.off('requestfinished', this._onFinished);
        this._page.off('requestfailed', this._onFinished);
    }
}

export default function (fastify, options, done) {
    fastify.get('/:type/:id', async (request, reply) => {
        let id = request.params.id;
        let type = request.params.type;
        let browser = null;
        let page = null;
        try {
            browser = await puppeteer.launch({ args: ['--disable-dev-shm-usage', '--no-sandbox'], acceptInsecureCerts: true, headless: 'new', slowMo: 250 });
            page = await browser.newPage();
            //await page.setRequestInterception(true);
            await page.setExtraHTTPHeaders({
                'Accept-Language': 'de-DE,de;q=0.9',
                'X-EXPORT-HEADER': Globals.exportHeader
            });
            //await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/127.0.0.0 Safari/537.36');
            /*page.on('request', request => {
                // Do nothing in case of non-navigation requests.
                if (!request.isNavigationRequest()) {
                    request.continue();
                    return;
                }
                // Add a new header for navigation request.
                const headers = request.headers();
                headers['X-EXPORT-HEADER'] = Globals.exportHeader;
                request.continue({ headers });
            });*/
            await page.setDefaultNavigationTimeout(0);
            await page.goto(Globals.frontendServerAddress + '/x/' + id);
            let size = null;
            let bodyHeight = null;
            let bodyWidth = null;
            if (type === "f") {
                //if freeform
                size = await freeformSize.query().where('no_pass_hash', id).first();
                if (Math.floor(size.max_width) && Math.floor(size.max_width) > 0 && Math.floor(size.max_height) && Math.floor(size.max_height) > 0) {
                    await page.setViewport({ width: Math.floor(size.max_width) + 10, height: Math.floor(size.max_height) + 10 + 60 });
                    let obj = { height: size.max_height, width: size.max_width };
                    await page.setViewport({ width: size.max_width, height:  size.max_height });
                    /*await page.evaluate(async (obj) => { 
                        window.scroll(obj.width, obj.height)
                        await new Promise((resolve, reject) => {
                            window.scroll(obj.width, obj.height)
                            resolve();
                        });
                    });*/
                }
                else {
                    await page.setViewport({ width: 8000, height: 8000 });
                    await page.evaluate(async () => {
                        window.scroll(8000, 8000)
                        await new Promise((resolve, reject) => {
                            window.scroll(8000, 8000)
                            resolve();
                        });
                    });
                }
            }
            else if (type === "s" || type === "t") {
                //if schedule or timeline
                await page.evaluate(async () => {
                    const style = document.createElement('style');
                    style.type = 'text/css';
                    const content = `
                    .column {
                        max-height:100%!important;
                    }
                    .post-content {
                        overflow: unset!important;
                    }
                    `;
                    style.appendChild(document.createTextNode(content));
                    const promise = new Promise((resolve, reject) => {
                        style.onload = resolve;
                        style.onerror = reject;
                    });
                    document.head.appendChild(style);
                    await promise;
                });
                if (type === "s") {
                    bodyWidth = (await page.evaluate(() => document.querySelector('#content') ? document.querySelector('#content').scrollWidth : ""))+96;
                    bodyHeight = (await page.evaluate(() => document.querySelector('#content') ? document.querySelector('#content').scrollHeight : ""))+80;
                } else {
                    bodyHeight = await page.evaluate(() => document.querySelectorAll('.post-content') ? (Math.max.apply(Math, Array.from(document.querySelectorAll('.post-content')).map(function (o) { return o.scrollHeight; }))) * 2 + 300 + 160 : 0);
                    bodyWidth = await page.evaluate(() => document.querySelector('#content') ? document.querySelector('#content').scrollWidth : "");
                }
                if (bodyHeight && bodyWidth) {
                    console.log({bodyWidth, bodyHeight});
                    await page.setViewport({ width: bodyWidth, height: bodyHeight });
                }
            };

            //await autoScroll(page);
            //await page.waitForTimeout(10000);
            await setTimeout(10000);
            /*await page.evaluate(async () => {
                const selectors = Array.from(document.querySelectorAll("img"));
                await Promise.all(selectors.map(img => {
                    if (img.complete) return;
                    return new Promise((resolve, reject) => {
                        img.addEventListener('load', resolve);
                        img.addEventListener('error', reject);
                    });
                }));
            })*/
            //await page.waitForTimeout(10000);
            await setTimeout(10000);
            let buffer = null;
            if (type === "f"){
                console.log({size});
                buffer = await page.screenshot({ clip: { x: 0, y: 0, width: Math.floor(size.max_width) + 10, height: Math.floor(size.max_height) + 10 + 60 } });
            }else if (type === "s" || type === "t") {
                buffer = await page.screenshot({ fullPage: true });
            }
            await page.close();
            await browser.close();
            const fileName = 'export.png';
            const fileType = 'image/png';
            const download = Buffer.from(buffer, 'base64');
            if (Globals.environment === 'development') {
                reply.header('Access-Control-Allow-Origin', '*');
                reply.header('Access-Control-Allow-Methods', 'GET');
            }
            reply.type(fileType)
                .header('Content-Disposition', `attachment; filename="${fileName}"`)
                .send(download);
        } catch (e) {
            console.error(e);
            if (page !== null) {
                await page.close();
            }
            if (browser !== null) {
                await browser.close();
            }
            return { status: 'error', msg: Globals.errorPicture };
        }

    });

    fastify.get('/:type/:id/pdf', async (request, reply) => {
        let id = request.params.id;
        let type = request.params.type;
        let layout = request.query.layout; //portrait or landscape
        let size = request.query.size; //A0 ... A4
        let comments = request.query.comments; //show comments
        let likes = request.query.likes; //show likes
        let browser = null;
        let page = null;
        try {
            browser = await puppeteer.launch({ acceptInsecureCerts: true, headless: true, args: ['--allow-file-access-from-files', '--enable-local-file-accesses', '--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'], protocolTimeout: 60000, timeout: 10000  });
            page = await browser.newPage();
            await page.setRequestInterception(true);
            page.on('request', request => {
                // Do nothing in case of non-navigation requests.
                if (!request.isNavigationRequest()) {
                    request.continue();
                    return;
                }
                // Add a new header for navigation request.
                const headers = request.headers();
                headers['X-EXPORT-HEADER'] = Globals.exportHeader;
                request.continue({ headers });
            });
            await page.setDefaultNavigationTimeout(0);
            let tracker = new InflightRequests(page);
            await page.goto(Globals.frontendServerAddress + '/pdf/' + id + "?layout=" + layout + "&size=" + size + "&comments=" + comments + "&likes=" + likes, { waitUntil: 'networkidle2', timeout: 10000 }).catch(e => {
                console.log('Navigation failed: ' + e.message);
            });
            let inflight = tracker.inflightRequests();
            await new Promise((resolve) => {
                let matches = Datagetting(inflight);
                let inv = setInterval(function () {
                    if (matches.length === 0) {
                        clearInterval(inv)
                        resolve();
                    } else {
                        inflight = tracker.inflightRequests();
                        matches = Datagetting(inflight);
                    }
                }, 2000);

                function Datagetting(inflight) {
                    return inflight.filter(element => {
                        if (element.toString().indexOf('data:') !== -1) {
                            console.log(element.url());
                            return true;
                        }
                    });
                }
            });

            tracker.dispose();
            await autoScroll(page);
            await page.evaluate(async () => {
                const selectors = Array.from(document.querySelectorAll("img"));
                await Promise.all(selectors.map(img => {
                    if (img.complete) return;
                    return new Promise((resolve, reject) => {
                        img.addEventListener('load', resolve);
                        img.addEventListener('error', reject);
                    });
                }));
            })
            await page.addStyleTag({
                content: `
                    @page { size: ${size} ${layout}; margin-top: 5mm; margin-bottom: 1cm; margin-left: 0.5cm; margin-right: 0.5cm }
                `,
            });
            await setTimeout(30000);
            await page.emulateMediaType('screen');
            let pageOpts = {
                //fullPage: true,
                displayHeaderFooter: true,
                headerTemplate: "<div><!-- no header --></div>",
                footerTemplate: "<div style='font-size: 2mm;width:100%;display:flex;justify-content:center;align-items:center'><div class='pageNumber'></div><div>/</div><div class='totalPages'></div></div>",
                //preferCSSPageSize: true,
                format: size,
                landscape: layout !== null && layout !== undefined && layout === "landscape" ? true : false
            };
            let buffer = await page.pdf(pageOpts);
            await page.close();
            await browser.close();
            const fileName = 'export.pdf';
            const fileType = 'application/pdf';
            const download = Buffer.from(buffer, 'base64');
            if (Globals.environment === 'development') {
                reply.header('Access-Control-Allow-Origin', '*');
                reply.header('Access-Control-Allow-Methods', 'GET');
            }
            reply.type(fileType)
                .header('Content-Disposition', `attachment; filename="${fileName}"`)
                .send(download);
        }
        catch (e) {
            console.error(e);
            if (page !== null) {
                await page.close();
            }
            if (browser !== null) {
                await browser.close();
            }
            return { status: 'error', msg: Globals.errorPDF };
        }
    });

    fastify.get('/chat/:id/pdf', async (request, reply) => {
        let id = request.params.id;
        let layout = request.query.layout; //portrait or landscape
        let size = request.query.size; //A0 ... A4
        let browser = null;
        let page = null;
        try {
            //const user = await adminPanel.query().leftJoin('ai_chat', 'ai_chat.user_mail', 'adminPanel.mail').select('adminPanel.mail', 'adminPanel.dnr', 'adminPanel.ai').where('ai_chat.uuid', id).first();
            const user = await aiChat.query().leftJoin('admin_panel', 'ai_chat.user_mail', 'admin_panel.mail').select('admin_panel.mail', 'admin_panel.dnr', 'admin_panel.ai').where('ai_chat.uuid', id).first();
            const token = await createToken(user.mail, user.dnr, user.ai);
            browser = await puppeteer.launch({ acceptInsecureCerts: true, headless: true, args: ['--allow-file-access-from-files', '--enable-local-file-accesses', '--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'], protocolTimeout: 60000, timeout: 10000 });
            page = await browser.newPage();
            await page.setRequestInterception(true);
            const expires = Math.floor(+new Date(token.date) / 1000);
            await page.setCookie({ name: 'Token', value: token.jwt, expires: expires, path: '/', domain: Globals.backendDomain });
            page.on('request', request => {
                // Do nothing in case of non-navigation requests.
                if (!request.isNavigationRequest()) {
                    request.continue();
                    return;
                }
                // Add a new header for navigation request.
                const headers = request.headers();
                headers['X-EXPORT-HEADER'] = Globals.exportHeader;
                request.continue({ headers });
            });
            page.on('console', msg => console.log('PAGE LOG:', msg.text()));
            await page.setDefaultNavigationTimeout(0);
            let tracker = new InflightRequests(page);
            await page.goto(Globals.frontendServerAddress + '/overview/chat/pdf/' + id + "?layout=" + layout + "&size=" + size, { waitUntil: 'networkidle2', timeout: 10000 }).catch(e => {
                console.error('Navigation failed: ' + e.message);
                throw e
            });
            let inflight = tracker.inflightRequests();
            await new Promise((resolve) => {
                let matches = Datagetting(inflight);
                let inv = setInterval(function () {
                    if (matches.length === 0) {
                        clearInterval(inv)
                        resolve();
                    } else {
                        inflight = tracker.inflightRequests();
                        matches = Datagetting(inflight);
                    }
                }, 2000);

                function Datagetting(inflight) {
                    return inflight.filter(element => {
                        if (element.toString().indexOf('data:') !== -1) {
                            console.log(element.url());
                            return true;
                        }
                    });
                }
            });

            tracker.dispose();
            await autoScroll(page);
            await page.evaluate(async () => {
                const selectors = Array.from(document.querySelectorAll("img"));
                await Promise.all(selectors.map(img => {
                    if (img.complete) return;
                    return new Promise((resolve, reject) => {
                        img.addEventListener('load', resolve);
                        img.addEventListener('error', reject);
                    });
                }));
            })

            await page.addStyleTag({
                content: `
                    @page { size: ${size} ${layout}; margin-top: 5mm; margin-bottom: 1cm; margin-left: 0.5cm; margin-right: 0.5cm }
                `,
            });

            await setTimeout(30000);
            await page.emulateMediaType('screen');
            let pageOpts = {
                //fullPage: true,
                displayHeaderFooter: true,
                headerTemplate: "<div><!-- no header --></div>",
                footerTemplate: "<div style='font-size: 2mm;width:100%;display:flex;justify-content:center;align-items:center'><div class='pageNumber'></div><div>/</div><div class='totalPages'></div></div>",
                //preferCSSPageSize: true,
                format: size,
                landscape: layout !== null && layout !== undefined && layout === "landscape" ? true : false
            };
            let buffer = await page.pdf(pageOpts);
            await page.close();
            await browser.close();
            const fileName = 'export.pdf';
            const fileType = 'application/pdf';
            const download = Buffer.from(buffer, 'base64');
            if (Globals.environment === 'development') {
                reply.header('Access-Control-Allow-Origin', '*');
                reply.header('Access-Control-Allow-Methods', 'GET');
            }
            reply.type(fileType)
                .header('Content-Disposition', `attachment; filename="${fileName}"`)
                .send(download);
        }
        catch (e) {
            console.error(e);
            await page.deleteCookie({ name: 'Token', url: Globals.publicFrontendServerAdress, domain: new URL(Globals.publicFrontendServerAdress).hostname });
            if (page !== null) {
                await page.close();
            }
            if (browser !== null) {
                await browser.close();
            }
            return { status: 'error', msg: Globals.errorPDF };
        }
    });

    fastify.get('/chat/:id/:userId/pdf', async (request, reply) => {
        let id = request.params.id;
        let userId = request.params.userId;
        let layout = request.query.layout; //portrait or landscape
        let size = request.query.size; //A0 ... A4
        let browser = null;
        let page = null;
        try {
            browser = await puppeteer.launch({ acceptInsecureCerts: true, headless: true, args: ['--allow-file-access-from-files', '--enable-local-file-accesses', '--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'], protocolTimeout: 60000, timeout: 10000 });
            page = await browser.newPage();
            await page.setRequestInterception(true);
            page.on('request', request => {
                // Do nothing in case of non-navigation requests.
                if (!request.isNavigationRequest()) {
                    request.continue();
                    return;
                }
                // Add a new header for navigation request.
                const headers = request.headers();
                headers['X-EXPORT-HEADER'] = Globals.exportHeader;
                request.continue({ headers });
            });
            await page.setDefaultNavigationTimeout(0);
            let tracker = new InflightRequests(page);
            await page.goto(Globals.frontendServerAddress + '/chat/pdf/' + id + "/" + userId + "?layout=" + layout + "&size=" + size, { waitUntil: 'networkidle2', timeout: 10000 }).catch(e => {
                console.log('Navigation failed: ' + e.message);
            });
            let inflight = tracker.inflightRequests();
            await new Promise((resolve) => {
                let matches = Datagetting(inflight);
                let inv = setInterval(function () {
                    if (matches.length === 0) {
                        clearInterval(inv)
                        resolve();
                    } else {
                        inflight = tracker.inflightRequests();
                        matches = Datagetting(inflight);
                    }
                }, 2000);

                function Datagetting(inflight) {
                    return inflight.filter(element => {
                        if (element.toString().indexOf('data:') !== -1) {
                            console.log(element.url());
                            return true;
                        }
                    });
                }
            });
            tracker.dispose();
            await autoScroll(page);
            await page.evaluate(async () => {
                const selectors = Array.from(document.querySelectorAll("img"));
                await Promise.all(selectors.map(img => {
                    if (img.complete) return;
                    return new Promise((resolve, reject) => {
                        img.addEventListener('load', resolve);
                        img.addEventListener('error', reject);
                    });
                }));
            })

            await page.addStyleTag({
                content: `
                    @page { size: ${size} ${layout}; margin-top: 5mm; margin-bottom: 1cm; margin-left: 0.5cm; margin-right: 0.5cm }
                `,
            });

            await setTimeout(30000);
            await page.emulateMediaType('screen');
            let pageOpts = {
                //fullPage: true,
                displayHeaderFooter: true,
                headerTemplate: "<div><!-- no header --></div>",
                footerTemplate: "<div style='font-size: 2mm;width:100%;display:flex;justify-content:center;align-items:center'><div class='pageNumber'></div><div>/</div><div class='totalPages'></div></div>",
                //preferCSSPageSize: true,
                format: size,
                landscape: layout !== null && layout !== undefined && layout === "landscape" ? true : false
            };
            let buffer = await page.pdf(pageOpts);
            await page.close();
            await browser.close();
            const fileName = 'export.pdf';
            const fileType = 'application/pdf';
            const download = Buffer.from(buffer, 'base64');
            if (Globals.environment === 'development') {
                reply.header('Access-Control-Allow-Origin', '*');
                reply.header('Access-Control-Allow-Methods', 'GET');
            }
            reply.type(fileType)
                .header('Content-Disposition', `attachment; filename="${fileName}"`)
                .send(download);
        }
        catch (e) {
            console.error(e);
            if (page !== null) {
                await page.close();
            }
            if (browser !== null) {
                await browser.close();
            }
            return { status: 'error', msg: Globals.errorPDF };
        }
    });

    done()
}
