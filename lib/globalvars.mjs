// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import 'dotenv/config'

var Globals = {
    /* general */
    'port':typeof process.env.EXPORT_PORT!==undefined?process.env.EXPORT_PORT:8080,
    'frontendServerAddress':typeof process.env.FRONTEND_SERVER_ADRESS!==undefined?process.env.FRONTEND_SERVER_ADRESS:"",
    'exportHeader':typeof process.env.EXPORT_HEADER!==undefined?process.env.EXPORT_HEADER:"",
    'errorPicture':typeof process.env.EXPORT_ERROR_PICTURE!==undefined?process.env.EXPORT_ERROR_PICTURE:"",
    'errorPDF':typeof process.env.EXPORT_ERROR_PDF!==undefined?process.env.EXPORT_ERROR_PDF:"",
    'basePath':typeof process.env.EXPORT_BASE_PATH!==undefined?process.env.EXPORT_BASE_PATH:"",
    'publicFrontendServerAdress':typeof process.env.PUBLIC_FRONTEND_SERVER_ADRESS!==undefined?process.env.PUBLIC_FRONTEND_SERVER_ADRESS:"",
    'environment': typeof process.env.NODE_ENV!==undefined?process.env.NODE_ENV:"production",
    'backendDomain': typeof process.env.EXPORT_BACKEND_DOMAIN!==undefined?process.env.EXPORT_BACKEND_DOMAIN:"localhost",
}

export default Globals;