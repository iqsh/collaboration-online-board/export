// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Fastify from 'fastify';
import fastifyCors from '@fastify/cors';
import 'dotenv/config';

import index from './routes/index.mjs'
import Globals from './lib/globalvars.mjs';

const fastify = Fastify({
    logger: true,
    maxParamLength: 200,
    bodyLimit: 104857600
});

fastify.register(fastifyCors, {
    origin: Globals.frontendServerAddress,
    credentials: true
});

fastify.register(index, { prefix: '/api' });

fastify.get('/', async (request, reply) => {
    return { status: 'ok' }
});

const start = async () => {
    try {
      await fastify.listen({port: Globals.port, host: '0.0.0.0'});
    } catch (err) {
      fastify.log.error(err)
      process.exit(1)
    }
  }
  
  start()