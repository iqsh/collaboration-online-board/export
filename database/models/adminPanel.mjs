// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class adminPanel extends Model{
    static get tableName(){
        return 'view_adminpanel';
    }

    static get idColumn() {
        return 'mail';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['mail'],
        
            properties: {
                mail: { type: 'string'},
                dashboard: { type: 'string'},
                password: { type: 'string'},
                dnr: { type: 'string'},
                ai: { type: 'boolean'}
            }
        };
    }

}
export default adminPanel;