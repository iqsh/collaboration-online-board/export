// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class sessionStore extends Model{
    static get tableName(){
        return 'session_store';
    }

    static get idColumn() {
        return 'session_id';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['session_id'],
        
            properties: {
                session_id: { type: 'string'},
                mail: { type: 'string'},
                expire: { type: 'string'},
            }
        };
    }

}
export default sessionStore;