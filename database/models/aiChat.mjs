// Copyright © Institut für Qualitätsentwicklung an Schulen Schleswig-Holstein (IQSH)
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import Model from '../index.mjs';

class aiChat extends Model{
    static get tableName(){
        return 'ai_chat';
    }

    static get idColumn() {
        return 'uuid';
    }

    static get jsonSchema() {
        return {
            type: 'object',
        
            properties: {
                uuid: { type: 'string'},
                name: { type: 'string'},
                person_uuid: { type: ['string','null']},
                create_date: { type: 'string'},
                model: { type: 'string'},
                user_mail: { type: 'string'},
                type: { type: 'string'},
                meta: { type: 'object'},
                system_prompt: { type: 'string'}
            }
        };
    }

}
export default aiChat;